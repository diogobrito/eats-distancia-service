package br.com.caelum.eats.diastancia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


@EnableDiscoveryClient
@SpringBootApplication
public class EatsDiastanciaServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EatsDiastanciaServiceApplication.class, args);
	}

}
	